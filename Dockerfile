FROM nhinds/ubuntu-vagrant-systemd:16.04

RUN apt-get update --yes && \
    apt-get install \
        --yes \
        --no-install-recommends \
        ssh sudo iproute2 python3 ubuntu-minimal openssh-server && \
    sed -i 's/UsePAM yes/UsePAM no/' /etc/ssh/sshd_config
