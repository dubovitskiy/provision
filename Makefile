# Вырезать прошлое подключение по SSH к машине Vagrant, если есть
.repair-known-hosts:
	$(CURDIR)/scripts/repair_known_hosts.py

# Скопировать SSH-ключ на Vagrant-машину с дефолтными настройками
.ssh-copy-id:
	sshpass -p vagrant \
	ssh-copy-id vagrant@127.0.0.1 \
		-p 2222 \
		-o StrictHostKeyChecking=no

# Настроить SSH-подключение к машине Vagrant
.configure-vagrant-ssh: .repair-known-hosts .ssh-copy-id

# Запустить ansible-плейбук
play:
	ansible-playbook \
		--extra-vars "@$(CURDIR)/ansible/vars.yml" \
		--inventory $(CURDIR)/ansible/inventory.yml \
		$(CURDIR)/ansible/playbook.yml

# Запустить ansible-плейбук на машине Vagrant
play-vagrant: .configure-vagrant-ssh play

# Пересоздать машину Vagrant и запустить ansible-плейбук
replay: destroy up play-vagrant

# Выключить машину Vagrant
destroy:
	vagrant destroy --force

# Запустить машину Vagrant
up:
	vagrant up

.PHONY: \
	.repair_known_hosts \
	.ssh-copy-id \
	.configure-vagrant-ssh \
	play \
	replay \
	play-vagrant \
	up \
	destroy