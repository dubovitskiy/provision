#!/usr/bin/env python3
import os
import pathlib

KNOWN_HOSTS_FILE = pathlib.Path.home() / '.ssh' / 'known_hosts'

if not KNOWN_HOSTS_FILE.exists():
    os.exit(0) # nothing to cleanup

KNOWN_HOSTS_PERMISSIONS = KNOWN_HOSTS_FILE.stat().st_mode
PREFIX = '[127.0.0.1]:2222'

lines = []

with open(KNOWN_HOSTS_FILE, 'r') as f:
    for line in f:
        if not line.startswith(PREFIX):
            lines.append(line.strip())

with open(KNOWN_HOSTS_FILE, 'w') as f:
    for line in lines:
        f.write(line)

KNOWN_HOSTS_FILE.chmod(KNOWN_HOSTS_PERMISSIONS)